import os
from UnleashClient import UnleashClient

instance_id=os.getenv('INSTANCE_ID')

print("{}".format(instance_id))
client = UnleashClient(
    url='https://gitlab.com/api/v4/feature_flags/unleash/10921388',
    app_name='production',
    instance_id=instance_id,
    refresh_interval=2,
    disable_metrics=True,
    custom_headers={"UNLEASH-INSTANCEID": instance_id, "UNLEASH-APPNAME": "production", "Content-Type": "application/json"}
)
client.initialize_client()
print("{}".format(client.is_enabled("feature_production")))
