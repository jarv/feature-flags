#!/usr/bin/env ruby

require 'unleash'
require 'unleash/context'

@unleash = Unleash::Client.new( url: 'https://gitlab.com/api/v4/feature_flags/unleash/10921388', app_name: 'production',
  instance_id: ENV['INSTANCE_ID'],
  refresh_interval: 2,
  metrics_interval: 2,
  retry_limit: 2,
  log_level: Logger::DEBUG,
)

feature_name = "feature_production"
unleash_context = Unleash::Context.new
unleash_context.user_id = 123

sleep 1
1.times do
  if @unleash.is_enabled?(feature_name, unleash_context)
    puts "> #{feature_name} is enabled"
  else
    puts "> #{feature_name} is not enabled"
  end
  sleep 1
  puts "---"
  puts ""
  puts ""
end
